#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "CUnit.h"
#include "Basic.h"
#include "recipes.h"

void createBookTest();
void createRecipeTest(char *, int);
void createIngredientTest(char*, double); 
void createPantryTest();
void addRecipeTest(struct RecipeBook *, struct Recipe *);

void createBookTest01(void) {createBookTest(); }
void createRecipeTest01(void) {createRecipeTest("Mashed Taties", 10); }
void createIngredientTest01(void) {createIngredientTest("Butta", 1000); }
void createPantryTest01(void) {createPantryTest();}
void addRecipeTest01(void) {addRecipeTest(newBook(), newRecipe("Derped Beans", 1)); }


void createBookTest(){
  struct RecipeBook * actual = newBook();
  struct RecipeBook book = {0};
  struct RecipeBook * expected = &book;
  CU_ASSERT_EQUAL(actual->head, expected->head);
}

void createRecipeTest(char * name, int servings){
  struct Recipe * actual = newRecipe(name, servings);
  struct Recipe expected = {.name=name, .servings=servings};
  printf("newRecipe test for name ACTUAL: %s, EXPECTED: %s\n", actual->name, expected.name);
  CU_ASSERT_STRING_EQUAL(actual->name, expected.name);
  printf("newRecipe test for servings ACTUAL %d, EXPECTED: %d\n", actual->servings, expected.servings);
  CU_ASSERT_EQUAL(actual->servings, expected.servings);
}

void createIngredientTest(char * name, double caloriesPerGram){
  struct Ingredient * actual = newIngredient(name, caloriesPerGram);
  struct Ingredient expected = {.name=name, .caloriesPerGram=caloriesPerGram, .next=0, .quantity=0};
  printf("newIngredient test for name ACTUAL: %s, EXPECTED: %s\n", actual->name, expected.name);
  CU_ASSERT_STRING_EQUAL(actual->name, expected.name);
  printf("newIngredient test for caloriesPerGram ACTUAL: %f, EXPECTED: %f", actual->caloriesPerGram, expected.caloriesPerGram);
  CU_ASSERT_EQUAL(actual->caloriesPerGram, expected.caloriesPerGram);
  CU_ASSERT_EQUAL(actual->next, expected.next);
  CU_ASSERT_EQUAL(actual->quantity, expected.quantity);
}


void createPantryTest(){
  struct Pantry * actual = newPantry();
  struct Pantry pantry = {0};
  struct Pantry * expected = &pantry;
  CU_ASSERT_EQUAL(actual->head, expected->head);
}

void addRecipeTest(struct RecipeBook * book, struct Recipe * recipe){
  addRecipe(book, recipe);
  struct RecipeBook*  actual = book->head;
  struct Recipe * expected = recipe;
  CU_ASSERT_EQUAL(actual->head.name, expected->name);
}

int main()
{
   CU_pSuite pSuite = NULL;


   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

 
   pSuite = CU_add_suite("Suite_1", NULL, NULL);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

 
   if (
          (NULL == CU_add_test(pSuite, "createBookTest01", createBookTest01 )) 
	|| (NULL == CU_add_test(pSuite, "createRecipeTest01", createRecipeTest01)) 
	|| (NULL == CU_add_test(pSuite, "createIngredientTest01", createIngredientTest01)) 
	|| (NULL == CU_add_test(pSuite, "createPantryTest01", createPantryTest01))
	|| (NULL == CU_add_test(pSuite, "addRecipeTest01", addRecipeTest01))
)
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

  
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
 
   CU_cleanup_registry();
   return CU_get_error();
 }
   
