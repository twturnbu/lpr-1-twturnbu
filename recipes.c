#include<stdio.h>
#include<stdlib.h>
#include "recipes.h"

struct RecipeBook * newBook(){
  struct RecipeBook book = {0};
  struct RecipeBook * ptr = &book;
  return ptr;
}

struct Recipe * newRecipe(char * name, int servings){
  struct Recipe * ptr = malloc(sizeof(struct Recipe));
  ptr->name = name;
  ptr->servings = servings;
  return ptr;
}

struct Ingredient * newIngredient(char * name, double caloriesPerGram){
  struct Ingredient * ptr = malloc(sizeof(struct Ingredient));
  ptr->name = name;
  ptr->caloriesPerGram = caloriesPerGram;
  ptr-> next = 0;
  ptr->quantity = 0;
  return ptr;
}

struct Pantry * newPantry(){
  struct Pantry * ptr = malloc(sizeof(struct Pantry));
  ptr -> head = 0;
  return ptr;
}

void addRecipe(struct RecipeBook * book, struct Recipe * recipe){
  return;
}
