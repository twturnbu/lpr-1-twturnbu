#ifndef RECIPES_H
#define RECIPES_H

struct RecipeBook{
  struct Recipe * head;
};

struct Recipe{
  char * name;
  int servings;
  struct Recipe * next;
}; 

struct Ingredient{
  char * name;
  double caloriesPerGram;
  struct Ingredient * next;
  double quantity;  
};

struct Pantry{
  struct Ingredient * head;
};

struct RecipeBook * newBook();
struct Recipe * newRecipe(char * name, int servings);
struct Ingredient * newIngredient(char * name, double caloriesPerGram);
struct Pantry * newPantry();
void addRecipe(struct RecipeBook * book, struct Recipe * recipe);
#endif
